#!/bin/sh

set -e

sudo dnf remove docker \
                  docker-common \
                  docker-selinux \
                  docker-engine-selinux \
                  docker-engine

# Install docker-engine.
URL=https://download.docker.com
PASS=${URL}/linux/fedora/docker-ce.repo
sudo dnf install dnf-plugins-core
sudo dnf config-manager --add-repo ${PASS}

sudo dnf config-manager --set-enabled docker-ce-edge
sudo mkdir -p /var/lib/docker # docker-engine-selinux issue

sudo dnf install docker-ce
# sudo dnf install  docker-engine docker-compose
sudo systemctl start docker
sudo systemctl enable docker

# Add docker group for running docker without sudo.
if grep -q "docker" /etc/group
    then
        echo "group docker already exists"
    else
        sudo groupadd docker
    fi

sudo gpasswd -a "${USER}" docker

sudo dnf install docker-compose